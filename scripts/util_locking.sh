#!/bin/bash

lock () {
    # $1 name of lock directory
    # $2 time to wait
    
    until mkdir $1 &> /dev/null
    do
        sleep $2 &> /dev/null
    done
}

unlock () {
    # $1 name of lock directory
    rmdir $1 &> /dev/null
}

check_lock () {
    # $1 name of lock directory
    ls $1 &> /dev/null
}
