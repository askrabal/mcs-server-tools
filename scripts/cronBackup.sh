#!/bin/bash
# $1 is the server's directory name to backup ie jungle|test
DIR_NAME=$1

usage () {
    cat <<EOF

EOF
    exit 1
}

askBi () {
    local rval=$2
    local pval
    read -p $1 pval
    case $pval in
    y|Y|yes|YES|Yes)
        eval $rval=true
        ;;
    *)
        eval $rval=false
        ;;
    esac
}

if [[ -z $DIR_NAME || ! -d "../$DIR_NAME" ]]
then
    echo "$DIR_NAME must exist in ../" 1>&2
    usage
fi
IGN_TAR_FILE=".tarignore" 
if [[ -e $IGN_TAR_FILE ]]
then
    cat <( echo -e "Confirm this tarignore file is complete\n\n\n" ) $IGN_TAR_FILE | less
    askBi "continue?" ANS
    if [[ ! $ANS ]]
    then
        exit 1
    fi
    TAR_OPTS="$TAR_OPTS -X $IGN_TAR_FILE"
fi
# perl counts up on the same line just to show progress — really not necessary
perl -ne 'BEGIN{$i=0;print "0";}print "\r$i" . $i++;END{print "\r$i\n";}' \
         <( tar -cvzf "$(date '+%Y%m%d-%H%M')-$DIR_NAME.tar.gz" -C ../$DIR_NAME $TAR_OPTS . )

