#!/usr/bin/env python2 
from mcstatus import MinecraftServer as MCS
import sys

server = MCS.lookup(sys.argv[1])
times = []

i = 0
while i < 3:
    try:
        times.append(server.ping())
    except:
        pass
    i += 1

if len(times) == 0:
    sys.exit(-1)
else:
    print sum(times) / float(len(times))
    sys.exit(0)
